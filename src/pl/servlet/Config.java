package pl.servlet;

import java.util.Arrays;
import java.util.List;

public class Config {

    public final static String DBUSER = "root";
    public final static String DBPASS = "*******";
    public final static String DBDRIVER = "com.mysql.jdbc.Driver";

    public final static List<String> DBURLS = Arrays.asList("jdbc:mysql://localhost:3306/servletDb",
            "jdbc:mysql://localhost:3307/servletDb",
            "jdbc:mysql://localhost:3308/servletDb",
            "jdbc:mysql://localhost:3309/servletDb",
            "jdbc:mysql://localhost:3310/servletDb",
            "jdbc:mysql://localhost:3311/servletDb",
            "jdbc:mysql://localhost:3312/servletDb",
            "jdbc:mysql://localhost:3313/servletDb",
            "jdbc:mysql://localhost:3314/servletDb",
            "jdbc:mysql://localhost:3315/servletDb");
}

