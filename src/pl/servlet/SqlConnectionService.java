package pl.servlet;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

public class SqlConnectionService implements JDBCService {

    private Connection connObj = null;

    @Override
    public Connection connectDb(String dbURL) {
        try {
            Class.forName(Config.DBDRIVER);
            connObj = DriverManager.getConnection(dbURL, Config.DBUSER, Config.DBPASS);
        } catch (Exception exObj) {
            exObj.printStackTrace();
        }
        return connObj;
    }

    @Override
    public void disconnectDb(ResultSet rsObj, Statement stmtObj) {
        try {
            rsObj.close();
            stmtObj.close();
            connObj.close();
        } catch (Exception exObj) {
            exObj.printStackTrace();
        }
    }
}
