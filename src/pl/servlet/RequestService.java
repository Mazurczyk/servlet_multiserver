package pl.servlet;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.concurrent.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class RequestService {

    private static final int N_THREADS = 10;

    private CompletionService<ResultSet> completionService;

    public RequestService() {
        completionService = getCompletionService();
    }

    public CompletionService<ResultSet> getCompletionService(){
        Executor executor = Executors.newFixedThreadPool(N_THREADS);
        return new ExecutorCompletionService<ResultSet>(executor);
    }

    public void submitQueries() {
        Config.DBURLS.stream()
                .forEach(dbURL -> completionService.submit(new DbCallable(dbURL)));
    }

    public String getResults() {
        return IntStream.range(0, N_THREADS)
                .mapToObj(i -> {
                    try {
                        return completionService.take();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    return null;
                })
                .map(resultSetFuture -> {
                    try {
                        return resultSetFuture.get();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    } catch (ExecutionException e) {
                        e.printStackTrace();
                    }
                    return null;
                })
                .map(resultSet -> {
                    try {
                        return resultSet.getString("columnLabel");
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                    return null;
                })
                .collect(Collectors.joining(","));
    }
}
