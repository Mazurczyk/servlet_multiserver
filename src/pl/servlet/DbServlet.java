package pl.servlet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet("/DbServlet")
public class DbServlet extends HttpServlet {
    private static final long serialVersionUID = 5397405593623536498L;

    private RequestService requestService;

    public DbServlet() {
        requestService = new RequestService();
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        requestService.submitQueries();
        String results = requestService.getResults();

        PrintWriter out = response.getWriter();
        out.println(results);
    }
}
