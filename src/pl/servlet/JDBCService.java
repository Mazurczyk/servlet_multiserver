package pl.servlet;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

public interface JDBCService {

    Connection connectDb(String dbURL);

    void disconnectDb(ResultSet rsObj, Statement stmtObj);
}
