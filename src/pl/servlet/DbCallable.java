package pl.servlet;

import java.sql.ResultSet;
import java.sql.Statement;
import java.util.concurrent.Callable;

public class DbCallable implements Callable<ResultSet> {

    private final static String SELECT_QUERY = "SELECT * FROM servletDb.EmployeeTbl";

    private ResultSet rsObj = null;
    private Statement stmtObj = null;
    private JDBCService jdbcService;
    private String dbURL;

    public DbCallable(String dbURL) {
        jdbcService = new SqlConnectionService();
        this.dbURL = dbURL;
    }

    @Override
    public ResultSet call() {
        try {
            stmtObj = jdbcService.connectDb(dbURL).createStatement();
            rsObj = stmtObj.executeQuery(SELECT_QUERY);
        } catch (Exception exObj) {
            exObj.printStackTrace();
        } finally {
            jdbcService.disconnectDb(rsObj, stmtObj);
        }
        return rsObj;
    }
}